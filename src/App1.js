import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { Container } from 'reactstrap';
import NavBar from './components1/NavBar';
import Home from './pages1/Home';
import RegisForm from './pages1/RegisForm';
import ReviewForm from './pages1/ReviewForm';

const App = () => {
  return (
    <>
      <NavBar />
      <Container className="mt-5">
        <Switch>
          <Route path="/signUp">
            <RegisForm />
          </Route>
          <Route path="/review">
            <ReviewForm />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </Container>
    </>
  );
}

export default App;