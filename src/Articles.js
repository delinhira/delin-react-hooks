import Axios from "axios";
import { useEffect, useState } from "react";
import { Link, Route, Switch, useRouteMatch } from "react-router-dom";
import ArticleDetail from './ArticleDetail';

const Articles = () => {
    const [article, setArticle] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const match = useRouteMatch();

    useEffect(() => {
        setIsLoading(true);

        const url = "https://5fa4bcd2732de900162e85ef.mockapi.io/api/articles/";
        Axios.get(url)
            .then(res => {
                setArticle(res.data);
                setIsLoading(false)
            })
    }, [])

    return (
        <>
            <Switch>
                <Route path={`${match.path}/:id`}>
                    <ArticleDetail />
                </Route>
                <Route path={match.path}>
                    <h2>Articles</h2>
                    {isLoading && <p>Loading...</p>}
                    {!isLoading && article.map(data => (
                        <div key={data.id}>
                            <h3>{data.title}</h3>
                            <p>{data.sinopsis}</p>
                            <Link to={`${match.url}/${data.id}`}>Read more...</Link>
                            <hr />
                        </div>

                    ))}
                </Route>
            </Switch>

        </>
    );
}

export default Articles;