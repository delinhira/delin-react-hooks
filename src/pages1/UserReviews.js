import React, { useState } from 'react';
import { } from 'reactstrap';
import ReviewCard from '../components1/ReviewCard'

const UserReviews = () => {
    const [data, setData] = useState([])
    // let data = [];
    const url = "https://5fa4bcd2732de900162e85ef.mockapi.io/api/reviews";

    fetch(url)
        .then(res => res.json())
        .then(result => setData(result))
        .then(console.log(data))

    return (
        <>
            <ReviewCard id={data.id} rating={data.rating} review={data.review} />
        </>
    );
}

export default UserReviews;