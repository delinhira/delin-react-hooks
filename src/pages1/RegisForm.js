import React, { useState } from 'react';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';

const RegisForm = () => {
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [gender, setGender] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const registerUser = (e) => {
        console.log("register")
        e.preventDefault();

        const url = "https://5fa4bcd2732de900162e85ef.mockapi.io/api/register";
        const bodyData = {
            firstName: firstName,
            lastName: lastName,
            gender: gender,
            email: email,
            password: password
        }

        fetch(url, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(bodyData)
        })
            .then((res) => res.json())
            .then((res) => console.log(res))
    }

    const radioCheck = (e) => {
        console.log(e.target.value)
        console.log(gender)
    }

    return (
        <>
            <h3>Sign Up</h3>
            <Form onChange={radioCheck}>
                <FormGroup>
                    <Label for="firstName">First Name</Label>
                    <Input type="text" name="firstName" id="userFirstName" onChange={(e) => setFirstName(e.target.value)} />
                </FormGroup>
                <FormGroup>
                    <Label for="lastName">Last Name</Label>
                    <Input type="text" name="lastName" id="userLastName" onChange={(e) => setLastName(e.target.value)} />
                </FormGroup>
                <FormGroup tag="fieldset" onChange={(e) => setGender(e.target.value)}>
                    <Label>Gender</Label>
                    <FormGroup check>
                        <Label check>
                            <Input type="radio" name="gender" value="male" />Male</Label>
                    </FormGroup>
                    <FormGroup check>
                        <Label check>
                            <Input type="radio" name="gender" value="female" />Female</Label>
                    </FormGroup>
                </FormGroup>
                <FormGroup>
                    <Label for="email">Email</Label>
                    <Input type="email" name="email" id="userEmail" onChange={(e) => setEmail(e.target.value)} />
                </FormGroup>
                <FormGroup>
                    <Label for="password">Password</Label>
                    <Input type="password" name="password" id="userPassword" onChange={(e) => setPassword(e.target.value)} />
                </FormGroup>
                <Button type="button" onClick={registerUser}>Sign Up</Button>
            </Form>
        </>
    );
}

export default RegisForm;