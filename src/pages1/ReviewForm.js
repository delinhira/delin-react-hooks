import React, { useState } from 'react';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import UserReviews from './UserReviews'

const ReviewForm = () => {
    const [rating, setRating] = useState(0);
    const [review, setReview] = useState("");

    const submitReview = (e) => {
        console.log("submit review")
        e.preventDefault();

        const url = "https://5fa4bcd2732de900162e85ef.mockapi.io/api/reviews";
        const bodyData = {
            rating: rating,
            review: review
        }

        fetch(url, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(bodyData)
        })
            .then((res) => res.json())
            .then((res) => console.log(res))
    }

    return (
        <>
            <h3>Review</h3>
            <Form>
                <FormGroup>
                    <Label for="rating">Rating</Label>
                    <Input type="select" name="rating" id="ratingByUser"  onChange={(e) => setRating(e.target.value)}>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                    </Input>
                </FormGroup>
                <FormGroup>
                    <Label for="review">Review</Label>
                    <Input type="textarea" name="review" id="userReview" onChange={(e) => setReview(e.target.value)} />
                </FormGroup>
                <Button onClick={submitReview}>Submit Review</Button>
            </Form>

            <UserReviews />
        </>
    );
}

export default ReviewForm;