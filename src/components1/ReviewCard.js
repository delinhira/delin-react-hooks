import React from 'react';
import { Card, CardTitle, CardText, CardSubtitle } from 'reactstrap'

const ReviewCard = (props) => {
    const {id, rating, review} = props;

    return (
        <>
            <Card body>
                <CardTitle tag="h5">Review {id}</CardTitle>
                <CardSubtitle>{rating}</CardSubtitle>
                <CardText>{review}</CardText>
            </Card>
        </>
    );
}

export default ReviewCard;