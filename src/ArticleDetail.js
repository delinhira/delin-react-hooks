import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';

const ArticleDetail = () => {
    const [data, setData] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const { id } = useParams();

    useEffect(() => {
        setIsLoading(true);
        const url = `https://5fa4bcd2732de900162e85ef.mockapi.io/api/articles/${id}`
        // console.log(url);

        Axios.get(url)
            .then(res => {
                setData(res.data)
                setIsLoading(false)
            })
    }, [id]);

    return (
        <>
            {isLoading && <p>Loading...</p>}
            {!isLoading &&
                <div>
                    <h1>{data.title}</h1>
                    <p>{data.content}</p>
                    <Link to="/articles">Back</Link>
                </div>
            }
        </>
    );
}

export default ArticleDetail;