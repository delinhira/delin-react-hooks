import React from 'react';
import { Link, Switch, Route } from 'react-router-dom';
import Home from './Home';
import Articles from './Articles';
import { Container } from 'reactstrap';

const App2 = () => {
    return (
        <>
            <nav>
                <ul>
                    <li>
                        <Link to="/">Home</Link>
                    </li>
                    <li>
                        <Link to="/articles">Articles</Link>
                    </li>
                </ul>
            </nav>
            <Container>
                <Switch>
                    <Route path="/articles">
                        <Articles />
                    </Route>
                    <Route exact path="/">
                        <Home />
                    </Route>
                </Switch>
            </Container>
        </>
    );
}

export default App2;